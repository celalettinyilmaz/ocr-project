### Prerequisites

You will need Docker installed on your system and a command line editor.

```
Docker
Git Bash (on Windows)
Terminal (Linux or Mac)
```

### Installing and Running

You can clone this repository or download a zip file, build and run the Docker image.

```
$ docker build -t ocr-project .
$ docker run -d -p 5005:5005 ocr-project
```

Then open up browser to http://localhost:5005
