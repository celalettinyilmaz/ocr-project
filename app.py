from flask import Flask, render_template, request
from werkzeug import secure_filename
import os
import sys
from PIL import Image
import pytesseract
import argparse
import cv2
from pdf2image import convert_from_path


__author__ = 'Celalettin YILMAZ <larustr@gmail.com>'
__source__ = ''

app = Flask(__name__)
UPLOAD_FOLDER = './static/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

@app.route("/")
def index():
  return render_template("index.html")

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      filename = secure_filename(f.filename)
      filepath = os.path.join(app.config['UPLOAD_FOLDER'],filename)
      f.save(filepath)

      filename_new, file_extension = os.path.splitext(filepath)

      return_data = []

      if(file_extension == '.pdf'):
        i = 0
        pages = convert_from_path(filepath, 500)
        for page in pages:
          page_file = os.path.join(filename_new+"_page_{}.jpg".format(i))
          page.save(page_file, 'JPEG')
          item = image_reader(page_file)
          return_data.append(item)
          i = i + 1
      else:
        item = image_reader(filepath)
        return_data.append(item)

      
      return render_template("uploaded.html", return_data=return_data)

      

def image_reader(image_path):
      image = cv2.imread(image_path)
      gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
      gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

      gray = cv2.medianBlur(gray, 3)

      ofilename = os.path.join(app.config['UPLOAD_FOLDER'],"{}.png".format(os.getpid()))
      cv2.imwrite(ofilename, gray)
      
      text = pytesseract.image_to_string(Image.open(ofilename))
      
      os.remove(ofilename)

      return {'text':text, 'image_path':image_path}




if __name__ == '__main__':
  port = int(os.environ.get('PORT', 5005))
  app.run(debug=True, host='0.0.0.0', port=port)
  # app.run(host="0.0.0.0", port=5000, debug=True)
